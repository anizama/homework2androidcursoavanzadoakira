package com.example.opcionalrecycler.transporte

import java.io.Serializable

data class Transporte (val id:Int, val name:String,
                       val foto:Int): Serializable {
}

