package com.example.opcionalrecycler.transporte.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.example.opcionalrecycler.R
import com.example.opcionalrecycler.transporte.Transporte
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_transporte.view.*

class TransporteAdapter(var transportes : MutableList<Transporte>,val itemCallBackTransporte:(item:Transporte)->Unit):RecyclerView.Adapter<TransporteAdapter.TranporteAdapterViewHolder>() {





    lateinit var onItemClick:(item:Transporte)->Unit
    //Crear una clase interna viewholder
    class TranporteAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(transporte: Transporte){itemView.tvnombretransporte.text=transporte.name
        Picasso.get().load(transporte.foto).error(R.drawable.car6).into(itemView.imgtransporte)
        }


        //declarar variables
       // var imgTransporte: ImageView=itemView.findViewById(R.id.imgtransporte)
       // var tvTransporte: TextView=itemView.findViewById(R.id.tvnombretransporte)
        //val imgcarro: ImageView=itemView.findViewById(R.id.abc)
        //Pintar la vista que cree "view"
        //XML row transporte
        //recibe de la funcion onBind
       // fun bind(transporte:Transporte){
         //   itemView.tvnombretransporte.text=transporte.name

        //}

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TranporteAdapterViewHolder {
            //inflar una vista XML row_transporte
        val view:View=LayoutInflater.from(parent.context).inflate(R.layout.row_transporte,parent,false)
        return TranporteAdapterViewHolder(view)// ahora  tambien tiene la vista
    }

    override fun getItemCount(): Int {
        return transportes.size
    }

    override fun onBindViewHolder(holder: TranporteAdapterViewHolder, position: Int) {
        //Se ejecuta el numero de elementos que tiene la vista
        val transporte = transportes[position]//Ahora lo pasamos a la clase
       // holder.tvTransporte.text=transporte.name
       // Picasso.get().load(transporte.foto).error(R.drawable.car6).into(holder.imgTransporte)
       // holder.imgcarro.setOnClickListener {
         //   onItemClick(transporte)
        //}
        holder.bind(transporte)
        holder.itemView.setOnClickListener {
            println("hola")
            itemCallBackTransporte(transporte)
       }
    }
}