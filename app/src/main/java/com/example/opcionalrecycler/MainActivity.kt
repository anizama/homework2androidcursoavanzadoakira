package com.example.opcionalrecycler

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.opcionalrecycler.transporte.Transporte
import com.example.opcionalrecycler.transporte.adapter.TransporteAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.row_transporte.*

class MainActivity : AppCompatActivity() {
    //1.declarar lista
    //Lista creada vacia
    val transportes = mutableListOf<Transporte>()
    //7. crear variable adaptador
    lateinit var adaptador: TransporteAdapter// la variable la inicializara mas adelante

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        cargarinformacion()
        configuraradaptador()
    }

    private fun configuraradaptador() {
        adaptador=TransporteAdapter(transportes){
            //transporte ->
           // Log.i("carrito",transporte.name)
            val bundle = Bundle().apply {
                //putString("KEY_NOMBRE",it.name)
                //putInt("KEY_NOMBRE",it.foto)
                putSerializable("KEY_TRANSPORTE",it)
            }
            val intent=Intent(this,MostrarPantalla::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        } // inicializo mi adaptador
        //ligar mi adaptardor con el recycler XML
        recyclerviewtransportes.adapter=adaptador
        recyclerviewtransportes.layoutManager= LinearLayoutManager(this)

        //definir si es linear o grid el recycler

    }


    //adaptador.onItemClick={

        //  val bundle=Bundle()
        //  bundle.putString("Nombre", transportes.toString())
        //  val intent=Intent(this,MostrarPantalla::class.java)
        //  intent.putExtras(bundle)
        //  startActivity(intent)
    //}

    private fun cargarinformacion() {
        transportes.add(Transporte(1,"Formula1",R.drawable.car1))
        transportes.add(Transporte(2,"Formula2",R.drawable.car2))
        transportes.add(Transporte(3,"Formula3",R.drawable.car3))
        transportes.add(Transporte(4,"Formula4",R.drawable.car4))
        transportes.add(Transporte(5,"Formula5",R.drawable.car5))
        transportes.add(Transporte(6,"Formula6",R.drawable.car6))
    }


}