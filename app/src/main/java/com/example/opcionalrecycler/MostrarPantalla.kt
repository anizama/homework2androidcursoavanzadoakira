package com.example.opcionalrecycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.opcionalrecycler.transporte.Transporte
import kotlinx.android.synthetic.main.activity_mostrar_pantalla.*
import kotlinx.android.synthetic.main.destinoactivity.*

class MostrarPantalla : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mostrar_pantalla)

        // P2-5 PASAR EL OBJETO pokemon al detalle
        val bundle:Bundle?= intent.extras
        bundle?.let {
            val transporte =it.getSerializable("KEY_TRANSPORTE") as Transporte
                tvtitulonombre.text=transporte.name
            if (tvtitulonombre.text=="Formula1"){imageView1.setImageResource(R.drawable.car1)}
            if (tvtitulonombre.text=="Formula2"){imageView1.setImageResource(R.drawable.car2)}
            if (tvtitulonombre.text=="Formula3"){imageView1.setImageResource(R.drawable.car3)}
            if (tvtitulonombre.text=="Formula4"){imageView1.setImageResource(R.drawable.car4)}
            if (tvtitulonombre.text=="Formula5"){imageView1.setImageResource(R.drawable.car5)}
            if (tvtitulonombre.text=="Formula6"){imageView1.setImageResource(R.drawable.car6)}
             // transporte.name
            // transporte.foto
          //  transporte.placa
           // transporte.id
            println("${transporte.name}")
            println("${transporte.foto}") // pero si tengo mas atributos paso el objeto
        }
    }
}